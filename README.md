# Url shortening website

Using this project you can deploy your own url shortener in web

## Getting Started
### Prerequisites

For running this project you need Flask version 1.0.2. But it may work on earlier versions also.

For installing Flask type in bash
```
$ pip install flask==1.0.2
```

### Installing

First clone this repo using command

```
$ git clone https://gitlab.com/mihaylenko/url-shortener
```

Then open bash in this project directory and type command
```
$ python app.py
```

In web browser open `https://localhost:5000/` and use your website for shortening urls

### Using

Use your own version or [this example](https://url-shortener-task.herokuapp.com/)

Type link in form and click submit.
Use provided shortened link which redirects you to the site you needed.

All short links stored in database, so all the links you created are reusable.

## Author

* **Bohdan Mykhailenko** - [Mihaylenko](https://gitlab.com/mihaylenko)