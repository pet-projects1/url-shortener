import sqlite3
from sqlite3 import OperationalError
import random
import string

from flask import Flask, redirect, request, render_template

app = Flask(__name__)

host = 'https://localhost:5000/'#'https://url-shortener-task.herokuapp.com/'


class IDGenerator(object):
    ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyz"

    def __init__(self, length=5):
        self._alphabet_length = len(self.ALPHABET)
        self._id_length = length

    def _encode_int(self, n):
        # Adapted from:
        #   Source: https://stackoverflow.com/a/561809/1497596
        #   Author: https://stackoverflow.com/users/50902/kmkaplan

        encoded = ''
        while n > 0:
            n, r = divmod(n, self._alphabet_length)
            encoded = self.ALPHABET[r] + encoded
        return encoded

    def generate_id(self):
        """Generate an ID without leading zeros.

        For example, for an ID that is eight characters in length, the
        returned values will range from '10000000' to 'zzzzzzzz'.
        """

        start = self._alphabet_length**(self._id_length - 1)
        end = self._alphabet_length**self._id_length - 1
        return self._encode_int(random.randint(start, end))


def check_table_existence():
    create_table = """
        CREATE TABLE URLS_DATA(
        ID INT PRIMARY KEY,
        URL TEXT NOT NULL
        );
        """
    with sqlite3.connect('data.db') as connection:
        cursor = connection.cursor()
        try:
            cursor.execute(create_table)
        except OperationalError:
            pass


url_shorting = IDGenerator()


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        original_url = str.encode(request.form.get('url'))
        with sqlite3.connect('data.db') as connection:
            cursor = connection.cursor()
            while True:
                id = url_shorting.generate_id()
                result = cursor.execute('''
                SELECT URL FROM URLS_DATA WHERE ID=?
                ''', [host + id])
                result = result.fetchall()
                if result == []:
                    break
        short_url = host + id
        with sqlite3.connect('data.db') as connection:
            cursor = connection.cursor()
            result = cursor.execute("""
            INSERT INTO URLS_DATA (URL, ID) VALUES ( ? , ? )""", [original_url, short_url])
        return render_template('home.html', short_url=short_url)
    return render_template('home.html')


@app.route('/<short_url>')
def redirect_short_url(short_url):
    url = host
    with sqlite3.connect('data.db') as connection:
        cursor = connection.cursor()
        try:
            result = cursor.execute('''
            SELECT URL FROM URLS_DATA WHERE ID=?
            ''', [host + short_url])
            result = result.fetchall()
            if (result != []):
                url = result[0][0]
        except Exception as e:
            print(e)
    return redirect(url)


if __name__ == '__main__':
    check_table_existence()
    app.run()#debug=True
